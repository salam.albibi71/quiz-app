import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { QuestionsComponent } from './questions/questions.component';

const routes: Routes = [
  { path: "",component:WelcomePageComponent },
  { path: "welcome", component: WelcomePageComponent },
  { path: "questions", component: QuestionsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
